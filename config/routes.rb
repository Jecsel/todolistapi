Rails.application.routes.draw do
  
  resources :list, only:[:index, :create, :destroy, :update] do
    collection do
      get 'show'
    end
  end

end
