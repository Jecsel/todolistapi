class ListController < ApplicationController

    def index
        @list = List.all
        render json: @list
    end

    def show
        @list = List.all
        render json: @list
    end

    def destroy
        @patient = List.find(params[:id])
        @patient.destroy
    end

    def create
        List.create create_params
        render json: :created
    end

    def update
        list = List.find params[:id]
        list.update create_params
    end

    def create_params 
        #allow only expected parameters
        params.require( :list ).permit( :title, :description)
    end

    def update_params 
        #allow only expected parameters
        params.require( :list ).permit(:title, :description)
    end

end
